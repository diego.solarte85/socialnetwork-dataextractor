from igql import InstagramGraphQL
from collections import OrderedDict
import pandas as pd


def parse_instagram_data(post):

        post_dict = OrderedDict({
            'user_id': post.data['owner']['id'],
            'username': post.data['owner']['username'],
            'id': post.data['id'],
            # 'message': post.data['edge_media_to_caption']['edges'][0]['node']['text'],
            'created_time': post.data['taken_at_timestamp'],
            'type': post.data['__typename'],
            'image_url': post.data['display_url'],
            'shortcode': post.data['shortcode'],
            'url': 'https://www.instagram.com/p/%s/' % post.data['shortcode']

        })

        try:
            post_dict['message'] = post.data['edge_media_to_caption']['edges'][0]['node']['text']
        except Exception as err:
            print(err)
            post_dict['message'] = None

        if 'edge_media_preview_like' in post.data.keys():
            post_dict['likes_count'] = post.data['edge_media_preview_like']['count']

        if 'edge_media_to_comment' in post.data.keys():
            post_dict['comments_count'] = post.data['edge_media_to_comment']['count']

        if post.data['__typename'] == 'GraphVideo':
            post_dict['video_view_count'] = post.data['video_view_count']

        if 'location' in post.data.keys():
            if post.data['location'] is not None:
                post_dict['location_name'] = post.data['location']['name']
                post_dict['location_id'] = post.data['location']['name']

        return post_dict


def get_instagram_data(username):
    igql_api = InstagramGraphQL()
    user = igql_api.get_user(username)
    posts = []
    posts += user.timeline

    for media_list in user.iterate_more_timeline_media():
        posts += media_list

    posts_list = list(map(lambda p: parse_instagram_data(p), posts))
    posts_df = pd.DataFrame(posts_list)
    posts_df['message'] = posts_df.message.str.replace('\n', '')

    return posts_df.to_json(orient='table')
