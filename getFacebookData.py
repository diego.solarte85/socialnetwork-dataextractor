#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan  28 22:34:20 2019

@author: Diego Solarte
"""

import json
import time
import requests
import os
import unittest
from collections import OrderedDict


class FacebookScraper:
    '''
    FacebookScraper class to scrape facebook info
    '''

    def __init__(self, token, api_version='v3.2'):
        self.payload = {'access_token': token}
        self.base_url = 'https://graph.facebook.com'
        self.api_version = api_version

    def req(self, endpoint, uid, **kwargs):
        '''Base GET request to API
        '''
        for key in kwargs:
            self.payload[key] = kwargs[key]
        final_uri = '/'.join([self.base_url, self.api_version, uid, endpoint])
        res = requests.get(final_uri, params=self.payload)
        return res.json()

    @staticmethod
    def convert_to_epochtime(date_string):
        '''Enter date_string in 2000-01-01 format and convert to epochtime'''
        try:
            epoch = int(time.mktime(time.strptime(date_string, '%Y-%m-%d')))
            return epoch
        except ValueError:
            print('Invalid string format. Make sure to use %Y-%m-%d')
            quit()

    def get_username_id(self, username):
        '''Convert Facebook username to id
        '''
        user_data = requests.get('/'.join([self.base_url,
                                           self.api_version,
                                           username]),
                                 params={'access_token': self.payload['access_token']}).json()
        return user_data['id']


    def get_feed(self, fan_page, since_d, until_d, limit_d=10):
        '''Get feed post from a specified fan_page in a time period
        '''
        self.payload['fields'] = 'id,created_time,name,message,comments.summary(true),\
        shares,type,published,link,likes.summary(true),actions,place,tags,\
        object_attachment,targeting,feed_targeting,scheduled_publish_time,\
        backdated_time,description,\
        reactions.type(LOVE).limit(0).summary(true).as(love),\
        reactions.type(HAHA).limit(0).summary(true).as(haha), \
        reactions.type(WOW).limit(0).summary(true).as(wow), \
        reactions.type(SAD).limit(0).summary(true).as(sad),\
        reactions.type(ANGRY).limit(0).summary(true).as(angry),full_picture'

        since_epoch = self.convert_to_epochtime(since_d)
        until_epoch = self.convert_to_epochtime(until_d)
        posts_data = self.req('feed',
                              self.get_username_id(fan_page),
                              since=since_epoch,
                              until=until_epoch,
                              limit=limit_d)

        feed_list = []
        feed_list += posts_data['data']
        while 'next' in posts_data['paging']:
            # for post in posts_data['data']:
            #     feed_list.append(post)
            posts_data = (requests.get(
                          posts_data['paging']['next'])).json()
            feed_list += posts_data['data']

        return feed_list

    def parse_feed_post(self, post):

        try:
            post_type = post["type"]
        except KeyError:
            post_type = ""
        try:
            created_time = post["created_time"]
        except KeyError:
            created_time = ""
        try:
            message = post["message"].replace('\n','')
        except KeyError:
            message = ""
        try:
            name = post["name"]
        except KeyError:
            name = ""
        try:
            description = post["description"]
        except KeyError:
            description = ""
        try:
            actions_link = post["actions"][0]["link"]
        except KeyError:
            actions_link = ""
        try:
            actions_name = post["actions"][0]["name"]
        except KeyError:
            actions_name = ""
        try:
            share_count = post["shares"]["count"]
        except KeyError:
            share_count = ""
        try:
            comment_count = post["comments"]["summary"]["total_count"]
        except KeyError:
            comment_count = ""
        try:
            like_count = post["likes"]["summary"]["total_count"]
        except KeyError:
            like_count = ""
        try:
            reactions_love = post["love"]["summary"]["total_count"]
        except KeyError:
            reactions_love = ""
        try:
            reactions_haha = post["haha"]["summary"]["total_count"]
        except KeyError:
            reactions_haha = ""
        try:
            reactions_wow = post["wow"]["summary"]["total_count"]
        except KeyError:
            reactions_wow = ""
        try:
            reactions_sad = post["sad"]["summary"]["total_count"]
        except KeyError:
            reactions_sad = ""
        try:
            reactions_angry = post["angry"]["summary"]["total_count"]
        except KeyError:
            reactions_angry = ""
        try:
            full_picture = post["full_picture"]
        except KeyError:
            full_picture = ""

        return OrderedDict({
            'post_type': post_type,
            'created_time': created_time,
            'message': message,
            'name': name,
            'description': description,
            'actions_link': actions_link,
            'actions_name': actions_name,
            'share_count': share_count,
            'comment_count': comment_count,
            'like_count': like_count,
            'reactions_love': reactions_love,
            'reactions_haha': reactions_haha,
            'reactions_wow': reactions_wow,
            'reactions_sad': reactions_sad,
            'reactions_angry': reactions_angry,
            'full_picture': full_picture
        })

    def convert_feed_data(self, response_json_list):
        '''Parse all list of feed posts'''
        feed_list = list(map(lambda post: self.parse_feed_post(post),
                             response_json_list))

        return json.dumps(feed_list)

    # def convert_comments_data(self, response_json_list):
    #     '''This will get the list of people who commented on the post,
    #     which can be joined to the feed table by post_id. '''
    #     list_all = []
    #     for response_json in response_json_list:
    #         data = response_json["data"]
    #         # like_list = []
    #         for i in range(len(data)):
    #             likes_count = 0
    #             row = data[i]
    #             post_id = row["id"]
    #             try:
    #                 comment_count = row["comments"]["summary"]["total_count"]
    #             except KeyError:
    #                 comment_count = 0
    #             if comment_count > 0:
    #                 comments = row["comments"]["data"]
    #                 for comment in comments:
    #                     row_list = []
    #                     created_time = comment["created_time"]
    #                     message = comment["message"]
    #                     user_id = 'Null'
    #                     name = 'Null'
    #                     message_id = comment["id"]
    #                     row_list.extend((post_id, created_time, message, \
    #                                      user_id, name, message_id))
    #                     list_all.append(row_list)
    #
    #             # Check if the next link exists
    #             try:
    #                 next_link = row["comments"]["paging"]["next"]
    #             except KeyError:
    #                 next_link = None
    #                 continue
    #
    #             if next_link is not None:
    #                 r = requests.get(next_link.replace("limit=25", "limit=100"))
    #                 comments_data = json.loads(r.text)
    #                 while True:
    #                     for i in range(len(comments_data["data"])):
    #                         row_list = []
    #                         comment = comments_data["data"][i]
    #                         created_time = comment["created_time"]
    #                         message = comment["message"]
    #                         user_id = 'Null'
    #                         name = 'Null'
    #                         message_id = comment["id"]
    #                         row_list.extend((post_id, created_time, message, \
    #                                          user_id, name, message_id))
    #                         list_all.append(row_list)
    #                     try:
    #                         next = comments_data["paging"]["next"]
    #                         r = requests.get(next.replace("limit=25", "limit=100"))
    #                         comments_data = json.loads(r.text)
    #                     except KeyError:
    #                         print("Comments for the post {} completed".format(post_id))
    #                         break
    #     return list_all


class TestFacebookScraperCase(unittest.TestCase):
    def setUp(self,):
        self.fb_scraper = FacebookScraper(os.environ['FACEBOOK_TOKEN'])

    def test_convert_to_epochtime(self):
        self.assertEqual(FacebookScraper.convert_to_epochtime('2019-01-01'),
                         1546297200)

    def test_get_username_id(self):
        # self.assertEqual(self.fb_scraper.get_username_id('cocacolaperu'),
        #                  '327026330836426')
        self.assertEqual(self.fb_scraper.get_username_id('BBVAespana'),
                         '242465215866259')

    def test_get_feed_len(self):
        self.assertEqual(len(self.fb_scraper
                         .get_feed('BBVAespana',
                                   since_d='2019-01-01',
                                   until_d='2019-01-05')), 5)

        self.assertEqual(len(self.fb_scraper
                         .get_feed('BBVAespana',
                                   since_d='2019-01-01',
                                   until_d='2019-01-05',
                                   limit_d=2)), 5)


if __name__ == '__main__':

    unittest.main(verbosity=2)
