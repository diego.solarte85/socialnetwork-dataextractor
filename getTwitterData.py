import os
import tweepy
import pandas as pd
from collections import OrderedDict


# SET UP APP CREDENTIALS
app_id = os.environ['TWITTER_APP_ID']
app_secret = os.environ['TWITTER_APP_SECRET']
token = os.environ['TWITTER_TOKEN']
token_secret = os.environ['TWITTER_TOKEN_SECRET']


# INSTANTIATE API WITH WAIT TRUE
auth = tweepy.OAuthHandler(app_id, app_secret)
auth.set_access_token(token, token_secret)
api = tweepy.API(auth, wait_on_rate_limit=True)


def get_tweet_data(json_tweet):
    tweet_dict = OrderedDict({
        'username': json_tweet['user']['screen_name'],
        'tweet_id': json_tweet['id'],
        'message': json_tweet['text'],
        'created_time': json_tweet['created_at'],
        'link': 'https://twitter.com/%s/status/%s' % (
                                            json_tweet['user']['screen_name'],
                                            json_tweet['id']),
        'shares_count': json_tweet['retweet_count'],
        'likes_count': json_tweet['favorite_count']
    })
    return tweet_dict


def get_twitter_data(username):
    '''Get timeline data from username
    '''
    raw_tweets = []
    for page in tweepy.Cursor(api.user_timeline,
                              id=username,
                              count=200).pages():
        # process status here
        raw_tweets += page

    tweets = list(map(lambda x: x._json, raw_tweets))
    tweets_data = list(map(lambda t: get_tweet_data(t), tweets))
    tweets_df = pd.DataFrame(tweets_data)
    tweets_df['message'] = tweets_df.message.str.replace('\n', '')

    return tweets_df.to_json(orient='table')
